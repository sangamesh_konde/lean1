package com.sangamesh;





import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.dao.Address;
import com.dao.UserDetails;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
/**
 * Hello world!
 *
 */
public class App 
{
	private static SessionFactory sessionFactory;



	public static void main( String[] args )
	{


		Configuration config = new Configuration();
		sessionFactory = config.configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		UserDetails details = new UserDetails();
		Address address = new Address();
		address.setCity("Bidar");
		address.setState("Karnataka");
		
		details.setId(1);
		details.setName("Sangamesh");
		details.setHomeAddress(address);
		details.setOfficeAddress(address);
		
		UserDetails details2 = new UserDetails();
		Address address2 = new Address();
		address2.setCity("Bangalore");
		address2.setState("Karnataka");
		
		details2.setId(2);
		details2.setName("Kitty");
		details2.setHomeAddress(address2);
		details2.setOfficeAddress(address2);
		
		session.save(details);
		session.save(details2);
		transaction.commit();



	}
}
